# CommuterSails

A [Sails](http://sailsjs.org) application for prototyping a CTA (Chicago Transit Authority) ETA application.

This app is still a work in progress. Base functionality should allow application to give bus and train times for the respective stop. Further functionality should suggest CTA alerts and use the crowd to report real-time delays (similar to waze).