/**
 * CtaController
 *
 * @description :: Server-side logic for managing ctas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
module.exports = {

  eta: function(req, res) {
    var routeid = req.query != null && "routeid" in req.query ? req.query.routeid : 0;
    var stopid = req.query != null && "stopid" in req.query ? req.query.stopid : 0;

    if(routeid === 0 || stopid === 0) {
      res.send({error: 'Invalid request'});
    }

    CtaService.getEta(routeid, stopid, function(response) {
      res.send(response);
    });
  }

};

