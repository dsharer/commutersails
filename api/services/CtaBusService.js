var request = require('superagent');
var parseString = require('xml2js').parseString;
var moment = require('moment');

module.exports = {
  getEta: function(routeID, stopID, callbackFunc) {
    request
      .get(sails.config.cta.busUri + 'getpredictions')
      .query({key: sails.config.cta.busKey, rt: routeID, stpid: stopID})
      .end(function(resp) {
        if(resp !== null && resp !== undefined && "text" in resp) {
          parseString(resp.text, function(err, result) {
            if(!err && "bustime-response" in result && "prd" in result["bustime-response"]) {

              var resultArray = [];
              //Loop through CTA api results
              for(var i=0; i < result["bustime-response"].prd.length; i++) {
                if("prdtm" in result["bustime-response"].prd[i] && result["bustime-response"].prd[i].prdtm !== undefined && result["bustime-response"].prd[i].prdtm !== "") {
                  var predictionDate = moment(result["bustime-response"].prd[i].prdtm, ["YYYYMMDD HH:mm"]);
                  var nowDate = moment();

                  var minutes = predictionDate.diff(nowDate, 'minutes');

                  var ctaResponse = {};
                  ctaResponse.etaMinutes = minutes;

                  if("dly" in result["bustime-response"].prd[i]) {
                    ctaResponse.isDelayed = result["bustime-response"].prd[i].dly;
                  }
                  else {
                    ctaResponse.isDelayed = false;
                  }

                  resultArray.push(ctaResponse);
                }
              }

              callbackFunc(resultArray);
            }
            else {
              callbackFunc(parseError);
            }
          });
        }
        else {
          callbackFunc(serverError);
        }
      });
  }
};

var serverError = {error: 'Server error while parsing cta response.', statusCode:500};
var parseError = {error: 'Error parsing cta response.', statusCode:304};

