var request = require('superagent');
var parseString = require('xml2js').parseString;
var moment = require('moment');

module.exports = {
  getEta: function(routeid, stopid, callbackFunc) {

    //Test for train routeid
    var regExp = new RegExp("^[a-zA-Z]{2,}");

    if(regExp.test(routeid)) {
      getTrainEta(routeid, stopid, callbackFunc);
    }
    else {
      getBusEta(routeid, stopid, callbackFunc);
    }
  }
};

function getTrainEta(routeid, stopid, callbackFunc) {
  request
    .get(sails.config.cta.trainUri + 'ttarrivals.aspx')
    .query({key: sails.config.cta.trainKey, rt: routeid, stpid: stopid})
    .end(function(resp) {
      if(resp !== null && resp !== undefined && "text" in resp) {
        parseString(resp.text, function(err, result) {

          var returnArray = [];

          if("ctatt" in result && "eta" in result.ctatt && result.ctatt.eta instanceof Array) {
            for(var i=0; i < result.ctatt.eta.length; i++) {
              var thisArrayElement = result.ctatt.eta[i];

              if("arrT" in thisArrayElement && thisArrayElement.arrT instanceof Array && thisArrayElement.arrT.length > 0) {

                var predictionDate = moment(thisArrayElement.arrT[0], ["YYYYMMDD HH:mm"]);
                var nowDate = moment();

                var minutes = predictionDate.diff(nowDate, 'minutes');

                var ctaResponse = {};
                ctaResponse.etaMinutes = minutes;

                if("isDly" in thisArrayElement && thisArrayElement.isDly instanceof Array && thisArrayElement.isDly.length > 0) {
                  ctaResponse.isDelayed = thisArrayElement.isDly[0] == 0 ? false : true;
                }
                else {
                  ctaResponse.isDelayed = false;
                }

                returnArray.push(ctaResponse);
              }
            }
            callbackFunc(returnArray);
          }
          else {
            callbackFunc(returnArray);
          }
        });
      }
      else {
        callbackFunc({error: 'Error parsing cta response.', statusCode:500});
      }
    });
}

function getBusEta(routeid, stopid, callbackFunc) {
  request
    .get(sails.config.cta.busUri + 'getpredictions')
    .query({key: sails.config.cta.busKey, rt: routeid, stpid: stopid})
    .end(function(resp) {
      if(resp !== null && resp !== undefined && "text" in resp) {
        parseString(resp.text, function(err, result) {
          var resultArray = [];
          if(!err && "bustime-response" in result && "prd" in result["bustime-response"]) {

            //Loop through CTA api results
            for(var i=0; i < result["bustime-response"].prd.length; i++) {
              if("prdtm" in result["bustime-response"].prd[i] && result["bustime-response"].prd[i].prdtm !== undefined && result["bustime-response"].prd[i].prdtm !== "") {
                var predictionDate = moment(result["bustime-response"].prd[i].prdtm, ["YYYYMMDD HH:mm"]);
                var nowDate = moment();

                var minutes = predictionDate.diff(nowDate, 'minutes');

                var ctaResponse = {};
                ctaResponse.etaMinutes = minutes;

                if("dly" in result["bustime-response"].prd[i]) {
                  ctaResponse.isDelayed = result["bustime-response"].prd[i].dly;
                }
                else {
                  ctaResponse.isDelayed = false;
                }

                resultArray.push(ctaResponse);
              }
            }

            callbackFunc(resultArray);
          }
          else {
            callbackFunc(resultArray);
          }
        });
      }
      else {
        callbackFunc(serverError);
      }
    });
}

var serverError = {error: 'Server error while parsing cta response.', statusCode:500};
var parseError = {error: 'Error parsing cta response.', statusCode:304};
